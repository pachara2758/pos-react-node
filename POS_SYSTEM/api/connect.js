// const {Client} = require('pg');
// const conn = new Client({
//     host: 'localhost',
//     port: '5432',
//     database: 'db_workshop_pos',
//     user: 'postgres',
//     password: 'root'
// })

const { Sequelize } = require('sequelize');

const sequelize = new Sequelize('db_workshop_pos', 'postgres', 'root', {
    host: 'localhost',
    dialect: 'postgres',
    logging: false,
})

module.exports = sequelize;