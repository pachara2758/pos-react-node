const conn = require('../connect');
const { DataTypes } = require("sequelize");
const AdminModel = conn.define('admin', {
    id: {
        type: DataTypes.BIGINT,
        autoIncrement: true,
        primaryKey: true
    },
    name: {
        type: DataTypes.BIGINT
    },
    usr: {
        type: DataTypes.BIGINT
    },
    pwd: {
        type: DataTypes.BIGINT
    },
    level: {
        type: DataTypes.BIGINT
    },
    email: {
        type: DataTypes.BIGINT
    },
   
})

AdminModel.sync({ alter: true });

module.exports = AdminModel;