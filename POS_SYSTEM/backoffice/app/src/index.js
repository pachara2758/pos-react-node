import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import { createBrowserRouter, RouterProvider } from 'react-router-dom';
import Home from './pages/Home'
import ReportMember from "./pages/ReportMember";
import Admin from './pages/Admin';

const router = createBrowserRouter([
  {
    path: '/',
    element: <App />
  },
  {
    path: '/home',
    element: <Home />
  },
  {
    path: '/reportMember',
    element: <ReportMember />
  },
  {
    path: '/reportChangePackage',
    element: <ReportChangePackage />
  },
  {
    path: '/reportSumSalePerDay',
    element: <ReportSumSalePerDay />
  },
  {
    path: '/reportSumSalePerMonth',
    element: <ReportSumSalePerMonth />
  },
  {
    path: '/reportSumSalePerYear',
    element: <ReportSumSalePerYear />
  },
  {
    path: '/admin',
    element: <Admin />
  }
]);

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(<RouterProvider router={router} />);

reportWebVitals();
