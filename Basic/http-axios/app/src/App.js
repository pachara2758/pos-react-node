import axios from "axios";
import { useState, useEffect } from "react";
import Swal from 'sweetalert2';

function App() {
  const [employees, setEmployees] = useState([]);
  const [name, setName] = useState();
  const [phone, setPhone] = useState();
  const [email, setEmail] = useState();
  const [id, setId] = useState(0);
  useEffect(() => {
    fetchData();
    fetchDataSelect();
  }, []);
  const fetchData = async () => {
    await axios
      .get("http://localhost:3000/testConnect")
      .then((res) => {
        console.log(res.data);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const fetchDataSelect = async () => {
    try {
      await axios
        .get("http://localhost:3000/select")
        .then((res) => {
          setEmployees(res.data);
        })
        .catch((err) => {
          throw err.response.data;
        });
    } catch (e) {
      console.log(e.message);
    }
  };

  const handleSave = async (e) => {
    e.preventDefault(); // ป้องกันรีเฟรช 2 ครั้ง
    try {
      const payload = {
        name: name,
        phone: phone,
        email: email,
      };
      if (id === 0) {
        await axios
          .post("http://localhost:3000/insert", payload)
          .then((res) => {
            fetchDataSelect();
          })
          .catch((err) => {
            throw err.response.data;
          });
      } else {
        payload.id = id;

        await axios.put('http://localhost:3000/update', payload).then(res => {
          fetchDataSelect();
          setId(0);
          setName('');
          setPhone('');
          setEmail('');
        }).catch(err => {
          throw err.response.data;
        })
      }
    } catch (e) {
      console.loge(e.massage);
    }
  };

  const handleEdit = (item) => {
    setName(item.name);
    setPhone(item.phone);
    setEmail(item.email);
    setId(item.id);
  };

  const handleDelete = (item) => {
    try{
      Swal.fire({
        title: 'confirm delete',
        text: 'Are you sure for delete ?',
        icon: 'question',
        showCancelButton: true,
        showConfirmButton: true
      }).then(async res => {
        if(res.isConfirmed){
          await axios.delete('http://localhost:3000/delete/' + item.id).then(res => {
            fetchDataSelect();
          }).catch(err => {
            throw err.response.data;
          })
        }
      })

    } catch(err) {
      console.log(err.massage);
    }
  }

  return (
    <div className="App container">
      <div className="mt-3 mb-2">employees Manager</div>

      <form onSubmit={handleSave} className="card mb-3">
        <div className="card-body">
          <div className="mt-2">
            <label>name</label>
            <input
              value={name}
              onChange={(e) => setName(e.target.value)}
              className="form-control"
            ></input>
          </div>

          <div className="mt-2">
            <label>phone</label>
            <input
              value={phone}
              onChange={(e) => setPhone(e.target.value)}
              className="form-control"
            ></input>
          </div>

          <div className="mt-2">
            <label>email</label>
            <input
              value={email}
              onChange={(e) => setEmail(e.target.value)}
              className="form-control"
            ></input>
          </div>

          <div className="mt-2">
            <button
              onClick={handleSave}
              type="submit"
              className="btn btn-primary"
            >
              Save
            </button>
          </div>
        </div>
      </form>

      <table className="table table-bordered table-striped mb-3">
        <thead>
          <tr>
            <th className="text-start">id</th>
            <th className="text-start">name</th>
            <th className="text-start">phone</th>
            <th className="text-start">email</th>
            <th className="text-center" width="170px"></th>
          </tr>
        </thead>
        <tbody>
          {employees.map((item) => (
            <tr>
              <td className="text-start">{item.id}</td>
              <td className="text-start">{item.name}</td>
              <td className="text-start">{item.phone}</td>
              <td className="text-start">{item.email}</td>
              <td className="text-center">
                <button onClick={e => handleEdit(item)} className="btn btn-success" style={{ marginRight: "10px" }}>
                  edit
                </button>
                <button onClick={e => handleDelete(item)} className="btn btn-danger">delete</button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default App;
