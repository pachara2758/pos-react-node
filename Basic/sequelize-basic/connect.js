const { Sequelize } = require('sequelize');

const sequelize = new Sequelize('db_node_react', 'postgres', 'root', {
    host: 'localhost',
    dialect: 'postgres',
    logging: false
});

module.exports = sequelize;