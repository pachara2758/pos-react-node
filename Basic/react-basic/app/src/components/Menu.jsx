import { Link } from "react-router-dom";

function Menu() {

    return (
        <>
            {/* การใช้ Link มันจะไม่ refresh หน้าเหมือนกับการใช้ tag a */}
            <Link to='/'>Home</Link>
            <Link to='/myInput'>My Input</Link>
        </>
    )
}
export default Menu;