import { useEffect } from "react";
import Swal from 'sweetalert2'
function MyUseEffect() {
    useEffect(() => {
        console.log('start component');
    })

    const handleShowAlert = () => {
        Swal.fire({
            title: 'my title',
            text: 'my text',
            icon: 'success', // success, info, warning, danger, question
            timer: 2000,
        })
    }
    const showConfirmButton = () => {
        Swal.fire({
            title: 'Confirm',
            text: 'Are your sure?',
            icon: 'question',
            showCancelButton: true,
            cancelButtonText: 'ยกเลิก',
            cancelButtonColor: 'red',

            showConfirmButton: true,
            confirmButtonText: 'ยืนยัน',
            confirmButtonColor: 'green'
            
        }).then(res => {
            if(res.isConfirmed){
                console.log('click confirm');
            }
            else{
                console.log('click cancel')
            }
        })
    }
    return (
        <>
            <h3>Use Effect</h3>
            <button onClick={handleShowAlert}>Click for show SweetAlert</button>
            <button onClick={showConfirmButton}>Confirm Button</button>
        </>
    )
}
export default MyUseEffect;