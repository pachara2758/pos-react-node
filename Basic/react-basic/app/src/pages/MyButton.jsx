import { useState } from "react";

function MyButton() {
    let [x, setX] = useState(0);
    return (
        <>
            <div>x = {x}</div>
            <button onClick={e => setX(x++) }>Click Here</button>
        </>
    )
}

export default MyButton;