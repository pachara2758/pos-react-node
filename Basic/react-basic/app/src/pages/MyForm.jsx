function MyForm() {
    const save = (e) => {
        e.preventDefault(); // command for stop refresh console
        console.log('form submit');
    }

    return (
        <>
            <form onSubmit={save}>
                <input style={{ padding: '20px', color: 'blue', fontSize: '20px'}}></input>
                <button onClick={save} type="submit">Submit Data</button>
            </form>
        </>
    )
}

export default MyForm;